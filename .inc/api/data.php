<?php

/*
 * Jun 4, 2018 - raspi by munsking<munsking@munsking.com>
 * 
 * default
 * 
 * This file is part of raspi.
 * 
 * raspi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * raspi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with raspi.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once $_SERVER["DOCUMENT_ROOT"] . '/.inc/header.php';

if(isset($p["temp"])){
  $dbh = new PDO("mysql:host=localhost;dbname=web","web","wifi1234!");

  $stmt = $dbh->prepare("
    select
      id,
      unix_timestamp(timestamp) as timestamp,
      temperature
    from temperature
");

  if(!$stmt->execute()){
    var_dump($stmt->errorInfo());
  }else{
    $rid = 0;
    while($row = $stmt->fetchObject()){
      $xArr[] = $row->timestamp;
      $yArr[] = (float)$row->temperature/1000;
  //    $flotArr[] = array((int)$row->id,(float)$row->temperature/1000);
      $flotArr[] = array($row->timestamp*1000,(float)$row->temperature/1000);
      $rid++;
    }
  }
  if(isset($p["data"])){
    echo json_encode($flotArr);
  }
  if(isset($p["options"])){
    $retArr = array(
      "xmin" => min($xArr),
      "xmax" => max($xArr),
      "ymin" => min($yArr),
      "ymax" => max($yArr)
    );
    echo json_encode($retArr);
  }
}

//echo json_encode($p);