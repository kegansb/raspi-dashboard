<?
error_reporting(E_ALL);
ini_set("display_errors", 1);


$dbh = new PDO("mysql:host=localhost;dbname=web","web","wifi1234!");

$stmt = $dbh->prepare("
  select
    id,
    unix_timestamp(timestamp) as timestamp,
    temperature
  from temperature");

if(!$stmt->execute()){
  var_dump($stmt->errorInfo());
}else{
  $rid = 0;
  while($row = $stmt->fetchObject()){
    $xArr[] = $row->timestamp;
    $yArr[] = (float)$row->temperature/1000;
//    $flotArr[] = array((int)$row->id,(float)$row->temperature/1000);
    $flotArr[] = array($row->timestamp*1000,(float)$row->temperature/1000);
    $rid++;
  }
}
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<h1>raspi temp log</h1>
<div id="tempGraph" style="width:100%;height:500px;"></div>
<div id="tempTooltip"></div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="http://www.flotcharts.org/flot/jquery.flot.js"></script>
<script src="http://www.flotcharts.org/flot/jquery.flot.time.js"></script>
<script src="/.inc/js/graphs.js"></script>
</script>
</body>
